# Starlight Toolkit

Library for easier vertx application development.

- Dependency Injection 
- RestAPI server development using annotation
- Database Access
- Easy remote invocation in clustered deployment
- Centralize configuration
- Centralize logging


 