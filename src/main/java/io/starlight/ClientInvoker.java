package io.starlight;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;

/**
 *
 * @author denny
 */
public class ClientInvoker implements InvocationHandler {

    protected Class targetClass;
    protected String targetName;
    protected String name;
    
    public ClientInvoker(Class targetClass, String name) {
        this.targetClass = targetClass;
        targetName = targetClass.getCanonicalName();
        this.name = name;
    }
    
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        
        Future future = Future.future();
        
        JsonArray jsArg = new JsonArray();

        if (args != null) {
         
            for (int i = 0; i < args.length; i++) {
            
                if (args[i] == null)
                    jsArg.addNull();
                else {
                 
                    if (
                            (args[i] instanceof Integer)
                            || (args[i] instanceof Boolean)
                            || (args[i] instanceof String)
                            || (args[i] instanceof Double)
                            || (args[i] instanceof Float)
                            || (args[i] instanceof Enum)
                            || (args[i] instanceof Long)
                            || (args[i] instanceof JsonObject)
                        )
                        jsArg.add(args[i]);
                    else
                        jsArg.add(JsonObject.mapFrom(args[i]));
                }                    
            }
        }
        
        Vertx
            .currentContext()
            .owner()
            .eventBus()
            .send(targetName + "|" + name +  "." + method.getName(), 
                    jsArg, 
                    result -> {

                        if (result.succeeded()) {

                            JsonObject jsObj = (JsonObject) result.result().body();
                            Object retObj = jsObj.getValue("result");

                            if (retObj != null) {
                                
                                if (retObj instanceof JsonObject) {
                                    
                                    Class cls = null;
                                    
                                    if (method.getGenericReturnType() instanceof ParameterizedType) {

                                        if (((ParameterizedType) method.getGenericReturnType()).getActualTypeArguments().length > 0)
                                            cls = (Class) ((ParameterizedType) method.getGenericReturnType()).getActualTypeArguments()[0];
                                    }
                                    
                                    if (cls != null) {
                                        
                                        if (cls != JsonObject.class)
                                            retObj = ((JsonObject) retObj).mapTo(cls);
                                    }
                                    else
                                        retObj = null;
                                }
                                    
                            }

                            future.complete(retObj);
                        }                                
                        else
                            
                            future.fail(result.cause());
                    });
        
        return future;
    }    
}
