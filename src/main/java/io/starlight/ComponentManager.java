package io.starlight;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.reflections.Reflections;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author denny
 */
public class ComponentManager {

    // -----------
    
    static class ComponentRegistry {
        
        protected Object obj;
        protected String name;
        protected boolean service;
        protected boolean inited;

        public ComponentRegistry(Object obj, String name, boolean service, boolean inited) {
            
            this.obj = obj;
            this.name = name;
            this.service = service;
            this.inited = inited;
        }

        public Object getObj() {
            return obj;
        }

        public void setObj(Object obj) {
            this.obj = obj;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isService() {
            return service;
        }

        public void setService(boolean service) {
            this.service = service;
        }

        public boolean isInited() {
            return inited;
        }

        public void setInited(boolean inited) {
            this.inited = inited;
        }
    }
        
    protected static boolean inited = false;
    protected static ConcurrentLinkedQueue<ComponentRegistry> componentList = new ConcurrentLinkedQueue();
    protected static ConcurrentLinkedQueue<PluginHandler> pluginList = new ConcurrentLinkedQueue();
    protected static ConcurrentHashMap<String, Object> proxyList = new ConcurrentHashMap();
    
    public static void addComponent(Object obj, String name) {
        
        componentList.add(new ComponentRegistry(obj, name, false, false));
    }
    
    public static void initVerticle(StarlightVerticle verticle) {
      
        List<String> scanList = new ArrayList<>();
        ComponentScan[] componentScanList =  verticle.getClass().getAnnotationsByType(ComponentScan.class);

        for (ComponentScan comp : componentScanList) {

            scanList.add(comp.value());
        }

        if (scanList.isEmpty()) {

            if (verticle.getClass().getPackage() != null)
                scanList.add(verticle.getClass().getPackage().getName());
        }

        for (String pkgName : scanList) {

            scanDir(pkgName);
        }

        applyPlugin(verticle);
        
        List<Object> initList = new ArrayList<>();
        
        // wire @Config & @AutoWired
        for (ComponentRegistry reg : componentList) {

            if (!reg.isInited()) {
             
                applyConfigAndWire(reg.getObj());
                reg.setInited(true);
                
                initList.add(reg.getObj());
            }
        }

        applyConfigAndWire(verticle);        
                
        // call init
        for (Object obj : initList) {

            callInit(obj);
        }
        
        callInit(verticle);
    } 
    
    protected static void applyPlugin(StarlightVerticle verticle) {
        
        // starlightpplugin
        
        Reflections reflections = new Reflections("io.starlight");
                
        Set<Class<?>> classSet = reflections.getTypesAnnotatedWith(Plugin.class);
                
        for (Class cls : classSet) {
                    
            boolean create = true;

            for (PluginHandler handler : pluginList) {

                if (handler.getClass().equals(cls)) {

                    create = false;
                    break;
                }
            }

            if (create) {

                try {

                    Constructor<?> ctor = cls.getConstructor();

                    if (ctor != null) {

                        Object obj = ctor.newInstance();

                        if (obj instanceof PluginHandler) 
                            pluginList.add((PluginHandler) obj);
                    }
                }
                catch (Exception e) {

                }
            }
        }
                
        // scan for other plugin
        Annotation[] annotList = verticle.getClass().getAnnotations();
        
        for (Annotation annot : annotList) {

            String dirName = annot.annotationType().getPackage().getName();
            
            if (!"io.startlight".equals(dirName)) {

                reflections = new Reflections(dirName);
                classSet = reflections.getTypesAnnotatedWith(Plugin.class);
        
                for (Class cls : classSet) {
                    
                    boolean create = true;
                    
                    for (PluginHandler handler : pluginList) {
                        
                        if (handler.getClass().equals(cls)) {
                            
                            create = false;
                            break;
                        }
                    }
                    
                    if (create) {
                        
                        try {
                            
                            Constructor<?> ctor = cls.getConstructor();

                            if (ctor != null) {

                                Object obj = ctor.newInstance();
                                
                                if (obj instanceof PluginHandler) 
                                    pluginList.add((PluginHandler) obj);
                            }
                        }
                        catch (Exception e) {
                            
                        }
                    }
                }
            }
        }
        
        // apply plugin list
        for (PluginHandler handler : pluginList) {
            
            handler.initVerticle(verticle);
        }
    }
    
    protected static boolean isCreated(Class<?> componentClass) {
        
        boolean result = false;
        
        for (ComponentRegistry reg : componentList) {
            
            if (reg.getObj().getClass().equals(componentClass)) {
                
                result = true;
                break;
            }
        }
        
        return result;
    }
    
    protected static void scanDir(String dir) {
    
        Reflections reflections = new Reflections(dir);
        
        // component
        Set<Class<?>> classSet = reflections.getTypesAnnotatedWith(Component.class);
        
        for (Class cls : classSet) {
            
            if (!isCreated(cls)) {
             
                try {
                
                    Constructor<?> ctor = cls.getConstructor();

                    if (ctor != null) {

                        Object obj = ctor.newInstance();

                        String name = null;
                        Component compAnot = (Component) cls.getAnnotation(Component.class);

                        if (compAnot != null)
                            name = compAnot.value();

                        componentList.add(new ComponentRegistry(obj, name, false, false));
                    }

                }
                catch (Exception e) {

                    System.out.println("Error initialize component : " + cls.getCanonicalName());
                    e.printStackTrace(System.out);
                }
            }            
        }
        
        // service
        classSet = reflections.getTypesAnnotatedWith(Service.class);
        
        for (Class cls : classSet) {
            
            if (!isCreated(cls)) {
                
                try {

                    Constructor<?> ctor = cls.getConstructor();

                    if (ctor != null) {

                        Object obj = ctor.newInstance();

                        String name = null;
                        Service svcAnot = (Service) cls.getAnnotation(Service.class);

                        if (svcAnot != null)
                            name = svcAnot.value();

                        componentList.add(new ComponentRegistry(obj, name, true, false));

                        // register remote proxy
                        Class<?>[] ifaceList = obj.getClass().getInterfaces();

                        for (Class<?> iface : ifaceList) {

                            if (isServiceInterface(iface)) {

                                register(iface, name, obj);
                            }
                        }
                    }

                }
                catch (Exception e) {

                    System.out.println("Error initialize service : " + cls.getCanonicalName());
                    e.printStackTrace(System.out);
                }
            }
        }
    }
    
    public static Object getComponent(Class<?> type) {
        
        return getComponent(type, "");
    }
    
    public static Object getComponent(Class<?> type, String name) {

        Object result = null;
        boolean hasCandidate = false;
        boolean found = false;
        
        for (ComponentRegistry reg : componentList) {

            if (!reg.isService() && type.isInstance(reg.getObj())) {

                hasCandidate = true;

                if (name.equals(reg.getName())) {

                    result = reg.getObj();
                    found = true;
                    break;
                }                        
            }
        }

        if (!found && hasCandidate) {

            for (ComponentRegistry reg : componentList) {

                if (!reg.isService() && type.isInstance(reg.getObj())) {

                    result = reg.getObj();
                }
            }
        }
        
        if (!found) {
            // mungkin maksudnya service

            if (isServiceInterface(type))
                result = getServiceClient(type, name);
        }
        
        return result;
    }
    
    protected static void applyConfigAndWire(Object target) {

        List<Field> fieldList = getAllFields(target.getClass());
        
        for (Field field : fieldList) {
            
            Config configAnot = field.getAnnotation(Config.class);
            
            if (configAnot != null) {
                                
                try {
                    
                    Object val = getSetting(configAnot.value(), field.getType());
                    
                    if (val != null) {
                        
                        field.setAccessible(true);
                        field.set(target, val);
                    }                        
                }
                catch (Exception e) {
                 
                    System.err.println("Error set field from config " + field.getName() + " : " + configAnot.value());
                    e.printStackTrace(System.out);
                }
            }
            else {

                AutoWired wireAnot = field.getAnnotation(AutoWired.class);
            
                if (wireAnot != null) {

                    Object comp = getComponent(field.getType(), wireAnot.value());
                    
                    if (comp != null) {
                        
                        try {
                            field.setAccessible(true);
                            field.set(target, comp);
                        }
                        catch (Exception e) {

                            System.err.println("Error set autowired " + field.getName());
                            e.printStackTrace(System.out);
                        }
                    }
                }
            }
        }
    } 
        
    protected static void callInit(Object target) {
        
        List<Method> listMethod = getAllMethods(target.getClass());
        
        for (Method method : listMethod) {
            
            Init initAnot = method.getAnnotation(Init.class);
            
            if ((initAnot != null) && (method.getParameterCount() == 0)) {
                
                try {
                    
                    method.invoke(target);
                }
                catch (Exception e) {
                    
                    System.err.println("Error call init " + method.getName());
                    e.printStackTrace(System.out);
                }
            }
        }
    }
    
    public static <T> T getSetting(String name, Class<T> expectedClass) {
    
        return getSetting(name, 
                            Vertx.currentContext().config(),
                            expectedClass);
    }
    
    protected static <T> T getSetting(String name, JsonObject setting, Class<T> expectedClass) {
        
        Object result = null;
        
        int pos = name.indexOf(".");
        String currName = name;
        String nextName = "";
                
        if (pos > 0) {
            
            currName = name.substring(0, pos);
            nextName = name.substring(pos + 1);
        }
        
        try {
            
            if ("".equals(nextName)) {
                
                if ((expectedClass == Integer.class) || (expectedClass == int.class))
                    result = setting.getInteger(currName);
                else if (expectedClass == String.class)
                    result = setting.getString(currName);
                else if ((expectedClass == Boolean.class) || (expectedClass == boolean.class))
                    result = setting.getBoolean(currName);
                else if (expectedClass == JsonObject.class)
                    result = setting.getJsonObject(currName);
                else if (expectedClass == JsonArray.class)
                    result = setting.getJsonArray(currName);
            }
            else {
                
                JsonObject nextSetting = setting.getJsonObject(currName);
                
                if (nextSetting != null)
                    result = getSetting(nextName, 
                                        nextSetting, 
                                        expectedClass);
            }
        }
        catch (Exception e) {
            
            result = null;
        }
        
        return (T) result;
    }
    
    public static List<Field> getAllFields(Class<?> type) {
        
        List<Field> result = new ArrayList<>();
        
        result.addAll(Arrays.asList(type.getDeclaredFields()));

        if (type.getSuperclass() != null) {
            
            List<Field> parentFields = getAllFields(type.getSuperclass());
            result.addAll(parentFields);
        }
        
        return result;
    }
    
    protected static List<Method> getAllMethods(Class<?> type) {
        
        List<Method> result = new ArrayList<>();
        
        result.addAll(Arrays.asList(type.getDeclaredMethods()));

        if (type.getSuperclass() != null) {
            
            List<Method> parentMethods = getAllMethods(type.getSuperclass());
            result.addAll(parentMethods);
        }
        
        return result;
    }
        
    protected static boolean isServiceInterface(Class<?> type) {
        
        boolean result = true;
        
        List<Method> methodList = getAllMethods(type);
        
        for (Method method : methodList) {
            
            if (!method.getReturnType().isAssignableFrom(Future.class)) {
                
                result = false;
                break;
            }
        }
       
        return result;
    }
    
    protected static void register(Class<?> type, String name, Object target) {

        if (name == null)
            name = "";

        String typeName = type.getCanonicalName();
        Method[] methods = type.getMethods();
        
        for (Method method : methods) {
         
            registerMethod(typeName, name, target, method);
        }        
    }
    
    protected static void registerMethod(String typeName, String name, Object target, Method method) {
        
        String channelName = typeName + "|" + name + "." + method.getName();
        System.err.println("register : " + channelName);

        Vertx
                .currentContext()
                .owner()
                .eventBus()
                .consumer(channelName, message -> {

                    System.out.println("calling : " + channelName);

                    JsonArray args = (JsonArray) message.body();

                    try {
                        ArrayList<Object> jsList = new ArrayList();
                        Object[] argsParam = new Object[method.getParameterCount()];

                        for (int i = 0; i < args.size(); i ++) {

                            Object jsObj = args.getValue(i);

                            if (jsObj instanceof JsonObject) {

                                jsObj = ((JsonObject) jsObj).mapTo(method.getParameterTypes()[i]);
                            }

                            argsParam[i] = jsObj;
                        }

                        ((Future<Object>) method.invoke(target, argsParam))
                                .setHandler(result -> {
                                    
                                    if (result.succeeded()) {

                                        JsonObject jsObj = new JsonObject();

                                        if (result.result() != null) {

                                            if (
                                                    result.result() instanceof Integer
                                                    || result.result() instanceof Float
                                                    || result.result() instanceof Long
                                                    || result.result() instanceof Double
                                                    || result.result() instanceof Boolean
                                                    || result.result() instanceof String
                                                    || result.result() instanceof Enum
                                                    || result.result() instanceof JsonObject
                                                    )
                                                jsObj.put("result", result.result());
                                            else
                                                jsObj.put("result", JsonObject.mapFrom(result.result()));                                                        
                                        }
                                        else
                                            jsObj.putNull("result");
                                        
                                        message.reply(jsObj);
                                    }                                        
                                    else
                                        message.fail(-1, result.cause().getMessage());
                                });                
                    }
                    catch (Exception e) {

                        message.fail(-1, e.getMessage());
                        e.printStackTrace(System.out);
                    }            
                });
    }
    
    public static <T> T getServiceClient(Class<T> type) {
    
        return getServiceClient(type, "");
    }
    
    public static <T> T getServiceClient(Class<T> type, String name) {
        
        if (name == null)
            name = "";
        
        T result = (T) proxyList.get(type.getCanonicalName() + "|" + name);

        if (result == null) {

            ArrayList<Class> interfaceList = new ArrayList<>(Arrays.asList(type.getInterfaces()));

            if (type.isInterface())
                interfaceList.add(type);

            result = (T) Proxy.newProxyInstance(type.getClassLoader(), 
                                interfaceList.toArray(new Class[interfaceList.size()]), 
                                new ClientInvoker(type, name));

            proxyList.put(type.getCanonicalName() + "|" + name, result);                
        }

        return result;
    }
}
