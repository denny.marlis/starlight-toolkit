package io.starlight;

import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;

/**
 *
 * @author denny
 */
public interface ConfigService {

    Future<JsonObject> getAllSetting();
    Future<JsonObject> getSetting(String name);
    Future<String> getString(String name);
    Future<Integer> getInt(String name);
    Future<Boolean> getBool(String name);
    
    Future<Boolean> set(String name, JsonObject obj);
    Future<Boolean> set(String name, int value);
    Future<Boolean> set(String name, String value);
}
