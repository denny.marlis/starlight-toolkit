package io.starlight;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

/**
 *
 * @author denny
 */
public class Launcher {

    public static void main(String[] args) {
    
        String mainVerticle = getMainVerticle();
        
        if (mainVerticle == null) {
            
            if (args.length > 0)
                mainVerticle = args[0];
        }
        
        if (mainVerticle != null)
            new Launcher().start(mainVerticle);
        else {
            
            System.out.println("Main verticle not found");
        }
    }
    
    protected static String getMainVerticle() {
        
        String result = null;
        
        try {        
        
            Enumeration<URL> resources = Launcher.class.getClassLoader().getResources("META-INF/MANIFEST.MF");
        
            while (resources.hasMoreElements()) {
            
                InputStream stream = resources.nextElement().openStream();
                Manifest manifest = new Manifest(stream);
                Attributes attributes = manifest.getMainAttributes();
        
                result = attributes.getValue("Main-Verticle");
            
                stream.close();
            }
        } 
        catch (IOException e) {
            
            throw new IllegalStateException(e.getMessage());
        }
        
        return result;
    }
    
    protected void start(String mainVerticle) {
        
        VertxOptions options = new VertxOptions();
        options.setClustered(true);
        
        Vertx.clusteredVertx(options, 
                vertxRet -> {
                    
                    // initial config from config.json if exist
                    File configFile = new File((new File("")).getAbsolutePath() + "/config.json");
                    
                    if (configFile.exists()) {
                        
                        Buffer buf = Vertx.currentContext().owner().fileSystem().readFileBlocking(configFile.getAbsolutePath());
                        String jsonStr = buf.toString();
                                                
                        JsonObject config = new JsonObject(jsonStr);
                        
                        Vertx.currentContext().config().mergeIn(config);
                    }
                    
                    ConfigService config = ComponentManager.getServiceClient(ConfigService.class);
        
                    // get initial config
                    config.getAllSetting().setHandler(
                        ret -> {

                            if (ret.succeeded())
                                Vertx.currentContext().config().mergeIn(ret.result());
                            
                            deployMain(mainVerticle);
                        });
                });        
    }
    
    protected void deployMain(String mainVerticle) {
        
        DeploymentOptions option = new DeploymentOptions();
        option.setConfig(Vertx.currentContext().config());
        
        Vertx.currentContext().owner().deployVerticle(mainVerticle, option);
    }    
}
