package io.starlight.db;

import io.vertx.core.CompositeFuture;
import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.sql.UpdateResult;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author denny
 */
public class CommonDAO {

    protected static Map<Class, List> fieldMap = new ConcurrentHashMap<>();
    protected static Map<Class, List> columnMap = new ConcurrentHashMap<>();
    
    protected Map<String, String> scriptMap = new ConcurrentHashMap<>();
    
    protected SQLClient sqlClient;
    protected JsonObject config;
    protected SQLDialect sqlDialect = SQLDialect.POSTGRESQL;
    
    protected static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    
    protected Vertx vertx() {
    
        return Vertx.currentContext().owner();
    }
    
    protected Context context() {
    
        return Vertx.currentContext();
    }
    
    protected Future<SQLConnection> getConn() {

        Future result = Future.future();
    
        if (sqlClient != null) {
            
            sqlClient.getConnection(connResult -> {
                
                    if (connResult.succeeded()) 
                        result.complete(connResult.result());
                    else
                        result.fail(connResult.cause());
                });
        }
        else
            result.fail("SQLClient not defined");
            
        return result;
    }
    
    protected static void setFieldValue(Object target, String fieldName, Object value) {
        
        String methodName = "set" + fieldName.toLowerCase();
        
        Method[] methodList = target.getClass().getMethods();
        boolean found = false;
        
        for (Method method : methodList) {
            
            if (method.getName().toLowerCase().equals(methodName)) {
                
                if (method.getParameterCount() == 1) {

                    found = true;
                
                    try {
                        if ((method.getParameterTypes()[0] == Date.class) && (value instanceof String)) {
                         
                            value = OffsetDateTime.parse(((String) value)).atZoneSameInstant(ZoneId.systemDefault()).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);                            
                            value = sdf.parse(((String )value).substring(0, 19));
                        }                            
                        
                        method.invoke(target, value);
                    }
                    catch (Exception e) {

                        System.out.println("Error setvalue " + fieldName);
                        e.printStackTrace(System.out);
                    }

                    break;
                } 
            }
        }
        
        if (!found) {
            
            List<Field> fields = getAllFields(target.getClass());
            
            for (Field field : fields) {
                
                field.setAccessible(true);
                
                Column colAnot = field.getAnnotation(Column.class);
                fieldName = fieldName.toLowerCase();
                        
                if (colAnot != null) {
                    
                    if (colAnot.value().toLowerCase().equals(fieldName)) {
                        
                        methodName = "set" + field.getName().toLowerCase();
                        
                        for (Method method : methodList) {
            
                            if (method.getName().toLowerCase().equals(methodName)) {

                                try {                                        
                                    method.invoke(target, value);
                                }
                                catch (Exception e) {

                                    System.out.println("Error setvalue " + fieldName);
                                    e.printStackTrace(System.out);
                                }

                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    
    protected void setFieldValues(Object target, List<String> names, List<Object> values) {

        int index = 0;
        
        while (index < names.size()) {
            
            setFieldValue(target, names.get(index), values.get(index));
            index++;
        }
    }
    
    protected Object getFieldValue(Field field, Object target) {
        
        Object result = null;
        
        String methodName = "get" + 
                            field.getName().substring(0, 1).toUpperCase() +
                            field.getName().substring(1);
  
        Method method = null;
        
        try {
            
            method = target.getClass().getMethod(methodName);
        }
        catch (Exception e) {
        }
                
        if ((method == null) && ((field.getType() == boolean.class) || (field.getType() == Boolean.class))) {

            methodName = "is" + 
                        field.getName().substring(0, 1).toUpperCase() +
                        field.getName().substring(1);

            try {
                method = target.getClass().getMethod(methodName);
            }
            catch (Exception e) {
                                
            }
        }   

        if (method != null) {
         
            try {
                
                result = method.invoke(target);
            }
            catch (Exception e) {
                
                System.out.println("Error getting field value : " + field.getName() + ": " + e.getMessage());                
            }
        }
        
        return result;
    }
    
    protected static List<String> getColumn(Class<?> type, Connection conn) {
    
        List<String> result = columnMap.get(type);
        
        if (result == null) {
            
            result = new ArrayList<>();
            
            try {
                Table tableAnnot = type.getAnnotation(Table.class);
                
                if (tableAnnot != null) {
                    
                    DatabaseMetaData metaData = conn.getMetaData();
                    ResultSet rs = metaData.getColumns(null, null, tableAnnot.value(), null);

                    while (rs.next()) {

                        result.add(rs.getString("COLUMN_NAME"));
                    }   
                }
            }
            catch (Exception e) {
                System.out.println("get metadata failed : " + e.getMessage());
                e.printStackTrace(System.out);
            }
            
            columnMap.put(type, result);
        }
        
        return result;
    }
    
    protected static List<Field> getAllFields(Class<?> type) {
        
        List<Field> fields = fieldMap.get(type);
        
        if (fields == null) {
            
            fields = new ArrayList<>();
        
            for (Class<?> c = type; c != null; c = c.getSuperclass()) {
                
                if (c != Object.class)
                    fields.addAll(Arrays.asList(c.getDeclaredFields()));
            }
            
            fieldMap.put(type, fields);
        }
        
        return fields;
    }
    
    public void init(JsonObject config) {
    
        this.config = config;
        sqlClient = JDBCClient.createShared(vertx(), config);
        
        try {
            
            loadScript(this.getClass().getSimpleName() + ".sql");
        }
        catch (Exception e) {
            
        }
    }
    
    protected <T> Future<Boolean> insert(final List<T> listObj) {
    
        Future result = Future.future();
        
        List<Future> listFut = new ArrayList<>();
        
        for (T obj : listObj) {
        
            listFut.add(insert(obj));
        }
                
        CompositeFuture.join(listFut)
                    .setHandler(ret -> {
                       
                        if (ret.succeeded())
                            result.complete(true);
                        else
                            result.fail(ret.cause());
                    });
        
        return result;
    }
    
    protected <T> Future<T> insert(final T obj) {
        
        Future result = Future.future();
        
        try {

            final Table tableAnnot = obj.getClass().getAnnotation(Table.class);

            if (tableAnnot != null) {

                String fieldList = "";
                String valuesList = "";

                List<Field> fields = getAllFields(obj.getClass());
                List<Object> paramList = new ArrayList<>();

                for (Field field : fields) {

                    field.setAccessible(true);

                    Computed computedAnot = field.getAnnotation(Computed.class);

                    if (computedAnot == null) {

                        AutoKey autokeyAnot = field.getAnnotation(AutoKey.class);

                        if (autokeyAnot == null) {

                            Column columnAnot = field.getAnnotation(Column.class);

                            if (columnAnot != null)
                                fieldList += ", " + columnAnot.value();
                            else
                                fieldList += ", " + field.getName();

                            valuesList += ", ?";
                            
                            Object val = getFieldValue(field, obj);
                            
                            if (val instanceof Date)
                                paramList.add(new Timestamp(((Date) val).getTime()));
                            else
                                paramList.add(val);
                        }
                    }
                }

                if (fieldList.length() > 0) {

                    fieldList = fieldList.substring(2);
                    valuesList = valuesList.substring(2);
                }

                final String sql = "insert into " 
                        + tableAnnot.value()  
                        + "(" + fieldList +  ")"
                        + " values " 
                        + "(" + valuesList +  ")";

                final JsonArray paramObj = new JsonArray(paramList);

                getConn()
                    .setHandler(connResult -> {

                        if (connResult.succeeded()) {

                            connResult.result().updateWithParams(sql, paramObj, upresult-> {

                                if (upresult.succeeded()) {

                                    if (upresult.result().getKeys().size() > 0) {

                                        Connection conn = connResult.result().unwrap();

                                        List<String> listColumn = getColumn(obj.getClass(), conn);

                                        int i = 0;
                                        for (Object val : upresult.result().getKeys()) {

                                            setFieldValue(obj, listColumn.get(i), val);
                                            i++;
                                        }

                                        result.complete(obj);
                                    }
                                    else
                                        result.complete(obj);
                                }                                    
                                else {

                                    System.out.println("failed : " + upresult.cause().getMessage());
                                    result.fail(upresult.cause());
                                }

                                connResult.result().close();
                            });
                        }
                        else {

                            System.out.println("failed conn : " + connResult.cause().getMessage());                                
                            result.fail(connResult.cause());
                        }                            
                    });
            }
            else
                throw new Exception("Table annotation not found : " + obj.getClass().getCanonicalName());
        }
        catch (Exception e) {

            result.fail(e);
        }
        
        return result;
    }
    
    protected <T> Future<Boolean> update(final List<T> listObj) {
    
        Future result = Future.future();
        
        List<Future> listFut = new ArrayList<>();
        
        for (T obj : listObj) {
        
            listFut.add(update(obj));
        }
                
        CompositeFuture.join(listFut)
                    .setHandler(ret -> {
                       
                        if (ret.succeeded())
                            result.complete(true);
                        else
                            result.fail(ret.cause());
                    });
        
        return result;
    }
                
    protected <T> Future<T> update(T obj) {
        
        Future<T> result = Future.future();
        
        try {

            final Table tableAnnot = obj.getClass().getAnnotation(Table.class);

            if (tableAnnot != null) {

                String sql = "update " + tableAnnot.value() + " set ";

                String updateList = "";
                String filterList = "";

                List<Object> paramUpdateList = new ArrayList<>();
                List<Object> paramFilterList = new ArrayList<>();

                List<Field> fields = getAllFields(obj.getClass());

                for (Field field : fields) {

                    field.setAccessible(true);

                    Computed computedAnot = field.getAnnotation(Computed.class);

                    if (computedAnot == null) {

                        Object fieldValue = getFieldValue(field, obj);

                        if (fieldValue != null) {

                            AutoKey autoAnot = field.getAnnotation(AutoKey.class);
                            Key keyAnot = field.getAnnotation(Key.class);

                            Column columnAnot = field.getAnnotation(Column.class);

                            if ((autoAnot != null) || (keyAnot != null)) {

                                if (columnAnot != null)
                                    filterList += " and " + columnAnot.value() + " = ? ";
                                else
                                    filterList += " and " + field.getName() + " = ? ";

                                paramFilterList.add(fieldValue);
                            }
                            else {

                                if (columnAnot != null)
                                    updateList += ", " + columnAnot.value() + " = ? ";
                                else
                                    updateList += ", " + field.getName() + " = ? ";

                                paramUpdateList.add(fieldValue);
                            }                                                                
                        }
                    }
                }

                if (paramUpdateList.size() > 0) {

                    sql += updateList.substring(1);

                    if (paramFilterList.size() > 0) {

                        sql += " where " + filterList.substring(4);
                        paramUpdateList.addAll(paramFilterList);
                    }

                    final JsonArray paramObj = new JsonArray(paramUpdateList);
                    final String querySQL = sql;

                    System.out.println(sql);

                    getConn()
                        .setHandler(connResult -> {

                            if (connResult.succeeded()) {

                                connResult.result().updateWithParams(querySQL, paramObj, queryResult-> {

                                    connResult.result().close();

                                    if (queryResult.succeeded())
                                        result.complete(obj);
                                    else {

                                        System.out.println("failed : " + queryResult.cause().getMessage());
                                        result.fail(queryResult.cause());
                                    }                                        
                                });
                            }
                            else {

                                System.out.println("failed conn : " + connResult.cause().getMessage());
                                result.fail(connResult.cause());
                            }                      
                        });
                }
                else
                    throw new Exception("Update field not found : " + obj.getClass().getCanonicalName());
            }
            else
                throw new Exception("Table annotation not found : " + obj.getClass().getCanonicalName());
        }
        catch (Exception e) {

            result.fail(e);
        }            
        
        return result;
    }

    protected <T> Future<Boolean> delete(final List<T> listObj) {
    
        Future result = Future.future();
        
        List<Future> listFut = new ArrayList<>();
        
        for (T obj : listObj) {
        
            listFut.add(delete(obj));
        }
                
        CompositeFuture.join(listFut)
                    .setHandler(ret -> {
                       
                        if (ret.succeeded())
                            result.complete(true);
                        else
                            result.fail(ret.cause());
                    });
        
        return result;
    }
    
    protected <T> Future<T> delete(T obj) {
        
        Future<T> result = Future.future();
        
        try {

            final Table tableAnnot = obj.getClass().getAnnotation(Table.class);

            if (tableAnnot != null) {

                String sql = "delete from " + tableAnnot.value();

                String filterList = "";
                List<Object> paramList = new ArrayList<>();

                List<Field> fields = getAllFields(obj.getClass());

                for (Field field : fields) {

                    field.setAccessible(true);

                    Computed computedAnot = field.getAnnotation(Computed.class);

                    if (computedAnot == null) {

                        Object fieldValue = getFieldValue(field, obj);

                        if (fieldValue != null) {

                           Column columnAnot = field.getAnnotation(Column.class);

                            if (columnAnot != null)
                                filterList += " and " + columnAnot.value() + " = ? ";
                            else
                                filterList += " and " + field.getName() + " = ? ";

                            paramList.add(fieldValue);
                        }
                    }
                }

                if (paramList.size() > 0) {

                    filterList = filterList.substring(4);
                    sql += " where " + filterList;
                }

                final JsonArray paramObj = new JsonArray(paramList);
                final String querySQL = sql;

                System.out.println(sql);

                getConn()
                    .setHandler(connResult -> {

                        if (connResult.succeeded()) {

                            connResult.result().updateWithParams(querySQL, paramObj, queryResult-> {

                                if (queryResult.succeeded()) 
                                    result.complete();
                                else {

                                    System.out.println("failed : " + queryResult.cause().getMessage());
                                    result.fail(queryResult.cause());
                                }

                                connResult.result().close();
                            });
                        }
                        else {

                            System.out.println("failed conn : " + connResult.cause().getMessage());                                
                            result.fail(connResult.cause());
                        }                            
                    });
            }
            else
                throw new Exception("Table annotation not found : " + obj.getClass().getCanonicalName());
        }
        catch (Exception e) {

            result.fail(e);
        }            
        
        return result;
    }

    protected <T> Future<List<T>> select(T obj) {
        
        Future<List<T>> result = Future.future();
        
        try {

            final Table tableAnnot = obj.getClass().getAnnotation(Table.class);

            if (tableAnnot != null) {

                String sql = "select * from " + tableAnnot.value();

                String filterList = "";
                List<Object> paramList = new ArrayList<>();

                List<Field> fields = getAllFields(obj.getClass());

                for (Field field : fields) {

                    field.setAccessible(true);

                    Computed computedAnot = field.getAnnotation(Computed.class);

                    if (computedAnot == null) {

                        Object fieldValue = getFieldValue(field, obj);

                        if (fieldValue != null) {

                           Column columnAnot = field.getAnnotation(Column.class);

                            if (columnAnot != null)
                                filterList += " and " + columnAnot.value() + " = ? ";
                            else
                                filterList += " and " + field.getName() + " = ? ";

                            paramList.add(fieldValue);
                        }
                    }
                }

                if (paramList.size() > 0) {

                    filterList = filterList.substring(4);
                    sql += " where " + filterList;
                }

                final JsonArray paramObj = new JsonArray(paramList);
                final String querySQL = sql;

                System.out.println(sql);

                getConn()
                    .setHandler(connResult -> {

                        if (connResult.succeeded()) {

                            connResult.result().queryWithParams(querySQL, paramObj, queryResult-> {

                                connResult.result().close();

                                if (queryResult.succeeded()) {

                                    try {
                                        List<T> resultList = (List<T>) getItems(obj.getClass(), 
                                                                                queryResult.result().getColumnNames(), 
                                                                                queryResult.result().getResults());
                                        result.complete(resultList);
                                    }
                                    catch (Exception e) {

                                        result.fail(e);
                                    }
                                }                    
                                else {

                                    System.out.println("failed : " + queryResult.cause().getMessage());
                                    result.fail(queryResult.cause());
                                }
                            });
                        }
                        else {

                            System.out.println("failed conn : " + connResult.cause().getMessage());                                
                            result.fail(connResult.cause());
                        }                            
                    });
            }
            else
                throw new Exception("Table annotation not found : " + obj.getClass().getCanonicalName());
        }
        catch (Exception e) {

            result.fail(e);
        }            
        
        return result;
    }
    
    protected <T> Future<T> selectOne(T obj) {
        
        Future<T> result = Future.future();
        
        select(obj).setHandler(resultSelect -> {
           
            if (resultSelect.succeeded()) {
                
                if (resultSelect.result().size() > 0)
                    result.complete(resultSelect.result().get(0));
                else
                    result.complete(null);
            }
            else
                result.fail(resultSelect.cause());
        });
        
        return result;
    }
    
    protected <T> List<T> getItems(Class<T> type, List<String> columns, List<JsonArray> rows) throws Exception {
        
        List<T> result = new ArrayList<>();

        Constructor<T> ctor = type.getConstructor();

        for (JsonArray line : rows) {

            T item = ctor.newInstance();

            setFieldValues(item, columns, line.getList());

            result.add(item);
        }                                                                                  
        
        return result;
    }
    
    protected <T> Future<List<T>> query(String sql, Class<T> type) {
        
        Future<List<T>> result = Future.future();
                    
        getConn()
            .setHandler(connResult -> {

                if (connResult.succeeded()) {

                    connResult.result().query(sql, queryResult-> {

                        if (queryResult.succeeded()) {

                            try {
                                List<T> resultList = getItems(type, queryResult.result().getColumnNames(), queryResult.result().getResults());
                                result.complete(resultList);
                            }
                            catch (Exception e) {
                                result.fail(e);
                            }
                        }                    
                        else {

                            System.out.println("query failed : " + queryResult.cause().getMessage());
                            result.fail(queryResult.cause());
                        }

                        connResult.result().close();
                    });
                }
                else {

                    System.out.println("failed conn : " + connResult.cause().getMessage());                                
                    result.fail(connResult.cause());
                }                            
            });
        
        return result;
    }

    protected <T> Future<List<T>> queryWithParam(String sql, Class<T> type, Object... objParams) {
        
        Future<List<T>> result = Future.future();
        
        Map<String, Object> map = new HashMap<>();

        int i = 0;

        while (i < objParams.length - 1) {

            if (objParams[i] instanceof String) {

                map.put((String) objParams[i], objParams[i +1]);
            }

            i += 2;
        }

        JsonArray params = new JsonArray();

        String targetSQL = sql;

        // parameter replace
        int currPos = 0;
        int startPos = targetSQL.indexOf("{{", currPos);

        while (startPos > 0) {

            int endPos = targetSQL.indexOf("}}");

            if (endPos > 0) {

                String key = targetSQL.substring(startPos + 2, endPos);

                if (map.containsKey(key)) {

                    targetSQL = targetSQL.substring(0, startPos) +
                                "?" +
                                targetSQL.substring(endPos + 2);

                    params.add(map.get(key));
                }
            }

            currPos = startPos + 1;
            startPos = targetSQL.indexOf("{{", currPos);
        }

        // literal replace
        currPos = 0;
        startPos = targetSQL.indexOf("[[", currPos);

        while (startPos > 0) {

            int endPos = targetSQL.indexOf("]]");

            if (endPos > 0) {

                String key = targetSQL.substring(startPos + 2, endPos);

                if (map.containsKey(key)) {

                    targetSQL = targetSQL.substring(0, startPos) +
                                map.get(key) +
                                targetSQL.substring(endPos + 2);
                }
            }

            currPos = startPos + 1;
            startPos = targetSQL.indexOf("[[", currPos);
        }

        System.out.println(targetSQL);

        final String execSQL = targetSQL;

        getConn()
            .setHandler(connResult -> {

                if (connResult.succeeded()) {

                    connResult.result().queryWithParams(execSQL, params, queryResult-> {

                        if (queryResult.succeeded()) {

                            connResult.result().close();

                            try {

                                List<T> resultList = getItems(type, 
                                                                queryResult.result().getColumnNames(),
                                                                queryResult.result().getResults());
                                result.complete(resultList);
                            }
                            catch (Exception e) {

                                result.fail(e);
                            }                                
                        }                                    
                        else {

                            System.out.println("failed : " + queryResult.cause().getMessage());
                            result.fail(queryResult.cause());
                        }
                    });
                }
                else {

                    System.out.println("failed conn : " + connResult.cause().getMessage());                                
                    result.fail(connResult.cause());
                }                            
            });
        
        return result;
    }

    protected <T> Future<T> querySingle(String sql, Class<T> type) {
        
        Future<T> result = Future.future();
        
        query(sql, type).setHandler(ret -> {
           
            if (ret.succeeded()) {
                
                if (ret.result().size() > 0)
                    result.complete(ret.result().get(0));
                else
                    result.fail(new Exception("Not found"));
            }
            else
                result.fail(ret.cause());
        });
        
        return result;
    }

    protected <T> Future<T> querySingleWithParam(String sql, Class<T> type, Object... params)  {
        
        Future<T> result = Future.future();
        
        queryWithParam(sql, type, params)
                .setHandler(resultSelect -> {
           
            if (resultSelect.succeeded()) {
                
                if (resultSelect.result().size() > 0)
                    result.complete(resultSelect.result().get(0));
                else
                    result.complete(null);
            }
            else
                result.fail(resultSelect.cause());
        });
        
        return result;
    }

    protected Future<UpdateResult> exec(String sql) {
        
        Future result = Future.future();
        
        getConn()
            .setHandler(connResult -> {

                if (connResult.succeeded()) {

                    connResult.result().update(sql, queryResult-> {

                        connResult.result().close();

                        if (queryResult.succeeded())
                            result.complete(queryResult.result());
                        else {

                            System.out.println("exec failed : " + queryResult.cause().getMessage());
                            result.fail(queryResult.cause());
                        }
                    });
                }
                else {

                    System.out.println("failed conn : " + connResult.cause().getMessage());                                
                    result.fail(connResult.cause());
                }                            
            });
        
        return result;
    }

    protected Future<UpdateResult> execWithParam(String sql, Object... objParams) {
        
        Future<UpdateResult> result = Future.future();
        
        Map<String, Object> map = new HashMap<>();

        int i = 0;

        while (i < objParams.length - 1) {

            if (objParams[i] instanceof String) {

                map.put((String) objParams[i], objParams[i +1]);
            }

            i += 2;
        }

        JsonArray params = new JsonArray();

        String targetSQL = sql;

        // parameter replace
        int currPos = 0;
        int startPos = targetSQL.indexOf("{{", currPos);

        while (startPos > 0) {

            int endPos = targetSQL.indexOf("}}");

            if (endPos > 0) {

                String key = targetSQL.substring(startPos + 2, endPos);

                if (map.containsKey(key)) {

                    targetSQL = targetSQL.substring(0, startPos) +
                                "?" +
                                targetSQL.substring(endPos + 2);

                    params.add(map.get(key));
                }
            }

            currPos = startPos + 1;
            startPos = targetSQL.indexOf("{{", currPos);
        }

        // literal replace
        currPos = 0;
        startPos = targetSQL.indexOf("[[", currPos);

        while (startPos > 0) {

            int endPos = targetSQL.indexOf("]]");

            if (endPos > 0) {

                String key = targetSQL.substring(startPos + 2, endPos);

                if (map.containsKey(key)) {

                    targetSQL = targetSQL.substring(0, startPos) +
                                map.get(key) +
                                targetSQL.substring(endPos + 2);
                }
            }

            currPos = startPos + 1;
            startPos = targetSQL.indexOf("[[", currPos);
        }

        System.out.println(targetSQL);

        final String execSQL = targetSQL;

        getConn()
            .setHandler(connResult -> {

                if (connResult.succeeded()) {

                    connResult.result().updateWithParams(execSQL, params, queryResult-> {

                        if (queryResult.succeeded()) {

                            connResult.result().close();
                            result.complete(queryResult.result());
                        }                                    
                        else {

                            System.out.println("failed : " + queryResult.cause().getMessage());
                            result.fail(queryResult.cause());
                        }
                    });
                }
                else {

                    System.out.println("failed conn : " + connResult.cause().getMessage());                                
                    result.fail(connResult.cause());
                }                            
            });
        
        return result;
    }

    protected void loadScript(String scriptLocation) throws Exception {
                    
        InputStream is = this.getClass().getResourceAsStream(scriptLocation);

        StringBuilder sb = new StringBuilder();
        char[] tmp = new char[4096];

        try {
            
            InputStreamReader reader = new InputStreamReader(is);
           
            for (int cnt; (cnt = reader.read(tmp)) > 0;)
                sb.append( tmp, 0, cnt );
            
        } 
        finally {
            is.close();
        }
        
        String script = sb.toString();
        
        Scanner scanner = new Scanner(script);
        
        String scriptName = "";
        String scriptText = "";
        
        while (scanner.hasNextLine()) {
            
            String line = scanner.nextLine();
            
            if (line.startsWith("--")) {
                
                if (scriptName.length() > 0)                    
                    scriptMap.put(scriptName, scriptText.substring(2));
                
                scriptName = line.substring(2);
                scriptText = "";
            }
            else
                scriptText += "\r\n" + line;
        }
        
        if (scriptName.length() > 0)                    
            scriptMap.put(scriptName, scriptText.substring(2));        
    }
    
    protected <T> Future<List<T>> queryScript(String scriptName, Class<T> type) {
        
        if (scriptMap.containsKey(scriptName))
            return query(scriptMap.get(scriptName), type);
        else
            return Future.failedFuture(new Exception("Script not found : " + scriptName));
    }
    
    protected <T> Future<List<T>> queryScriptWihtParam(String scriptName, Class<T> type, Object... params) {
        
        if (scriptMap.containsKey(scriptName))            
            return queryWithParam(scriptMap.get(scriptName), type, params);
        else
            return Future.failedFuture(new Exception("Script not found : " + scriptName));
    }
    
    protected <T> Future<T> queryScriptSingle(String scriptName, Class<T> type) {
                
        if (scriptMap.containsKey(scriptName))            
            return querySingle(scriptMap.get(scriptName), type);
        else
            return Future.failedFuture(new Exception("Script not found : " + scriptName));
    }
    
    protected <T> Future<T> queryScriptSingleWithParam(String scriptName, Class<T> type, Object... params) {
        
        if (scriptMap.containsKey(scriptName))
            return querySingleWithParam(scriptMap.get(scriptName), type, params);
        else
            return Future.failedFuture(new Exception("Script not found : " + scriptName));            
    }
    
    protected Future<UpdateResult> execScript(String scriptName) {
        
        if (scriptMap.containsKey(scriptName))            
            return exec(scriptMap.get(scriptName));
        else
            return Future.failedFuture(new Exception("Script not found : " + scriptName));
    }
    
    protected Future<UpdateResult> execScriptWithParam(String scriptName, Object... params) {
        
        if (scriptMap.containsKey(scriptName))             
            return execWithParam(scriptMap.get(scriptName), params);
        else
            return Future.failedFuture(new Exception("Script not found : " + scriptName));
    }
}
