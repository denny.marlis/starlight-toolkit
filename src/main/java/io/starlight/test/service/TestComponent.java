package io.starlight.test.service;

import io.vertx.core.Future;

/**
 *
 * @author denny
 */
public interface TestComponent {

    Future<Integer> testInt(int a);
    Future<Boolean> testBoolean(boolean a);
    Future<String> testString(String a, String b);
}
