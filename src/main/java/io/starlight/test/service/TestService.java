package io.starlight.test.service;

import io.vertx.core.Future;

/**
 *
 * @author denny
 */
public interface TestService {

    Future<Integer> svcInt(int a);
    Future<Boolean> svcBoolean(boolean a);
    Future<String> svcString(String a, String b);    
}
