package io.starlight.test.service;

import io.starlight.Service;
import io.vertx.core.Future;

/**
 *
 * @author denny
 */
@Service
public class TestServiceImpl implements TestService {

    @Override
    public Future<Integer> svcInt(int a) {
        
        return Future.succeededFuture(a + a);
    }

    @Override
    public Future<Boolean> svcBoolean(boolean a) {
        
        return Future.succeededFuture(!a);
    }

    @Override
    public Future<String> svcString(String a, String b) {
        
        return Future.succeededFuture(a + "-" + b + " Service");
    }
    
    
}
