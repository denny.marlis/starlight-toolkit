package io.starlight.test.service;

import io.starlight.AutoWired;
import io.starlight.ComponentScan;
import io.starlight.StarlightVerticle;

/**
 *
 * @author denny
 */
@ComponentScan("io.starlight.test.service")
@ComponentScan("io.starlight.test.serviceAlt")
public class TestServiceVerticle extends StarlightVerticle {

    @AutoWired
    protected TestComponent comp;
    
    // test alt
    @AutoWired("Alt")
    protected TestComponent compAlt;
    
    @AutoWired
    protected TestService service;
    
    // test alt
    @AutoWired("SvcAlt")
    protected TestService serviceAlt;
    
    @Override
    public void start() throws Exception {
        
        System.err.println("====== TestServiceVerticle start ====");
        System.err.println("component : " + comp);
        System.err.println("component alt : " + compAlt);
                
        System.err.println("testInt ===> " + comp.testInt(5));
        System.err.println("testString Alt ===> " + compAlt.testString("xx", "yy"));
        
        service.svcInt(5).setHandler(ret -> {
            
                if (ret.succeeded()) {
                    
                    System.out.println("svcInt : " + ret.result());
                }
                else {
                    
                    System.out.println("failed svcInt : " + ret.cause());
                }
            });
        
        serviceAlt.svcString("xx", "yy")
                    .setHandler(ret -> {
                        
                       if (ret.succeeded()) {

                            System.out.println("svcString : " + ret.result());
                        }
                        else {

                            System.out.println("failed svcString : " + ret.cause());
                        }                        
                    });
    }
}
