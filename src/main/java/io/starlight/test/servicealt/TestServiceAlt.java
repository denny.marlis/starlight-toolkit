package io.starlight.test.servicealt;

import io.starlight.Service;
import io.starlight.test.service.TestService;
import io.vertx.core.Future;

/**
 *
 * @author denny
 */
@Service("SvcAlt")
public class TestServiceAlt implements TestService {

    @Override
    public Future<Integer> svcInt(int a) {
        
        return Future.succeededFuture(a * a);
    }

    @Override
    public Future<Boolean> svcBoolean(boolean a) {
        
        return Future.succeededFuture(a);
    }

    @Override
    public Future<String> svcString(String a, String b) {
        
        return Future.succeededFuture(a + "-" + b + " -- Service alt");
    }
    
}
